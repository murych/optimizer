#ifndef VERTEX_H
#define VERTEX_H

#include <iostream>
#include <vector>

class Vertex
{
public:
  std::vector<double> m_coords;

  Vertex(){};
  //  Vertex(int n);
  Vertex(std::vector<double> coords);

  double dist(const Vertex& v);
  bool isNeg();

  Vertex operator+(const Vertex& v);
  Vertex operator-(const Vertex& v);
  Vertex operator*(const double d);
  Vertex operator/(const double q);
  Vertex operator+=(const Vertex& v);

  friend std::ostream& operator<<(std::ostream& out, const Vertex& vertex);
};

#endif // VERTEX_H
