#ifndef MINIMIZER_H
#define MINIMIZER_H

#include "dyes.h"
#include "simplex.h"
#include "vertex.h"

#include <numeric>

class Minimizer
{
private:
  double m_alpha, m_beta, m_gamma;
  double f_h, f_g, f_l, f_r, f_e, f_s, tempD;
  Vertex x_h, x_g, x_l, x_r, x_e, x_s, x_c, tempV;

public:
  Minimizer(double alpha = 1.0, double beta = 0.5, double gamma = 2.0)
    : m_alpha(alpha)
    , m_beta(beta)
    , m_gamma(gamma)
  {}

  bool quitCase(Simplex* s);
  void sort(Simplex* simplex, std::vector<double> f);

  Simplex NMA(spectre1d_t mix, std::vector<Dye> dyes, Simplex* simplex);
  Simplex NMA(Simplex* simplex);
};

#endif // MINIMIZER_H
