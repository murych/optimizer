#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <chrono>

#include "dyes.h"
#include "gauss.h"
#include "minimizer.h"

//#include <gsl/gsl_multimin.h>

using namespace std;

int
main()
{
//  cout << std::scientific;
//#define TEST
#ifdef TEST
  Vertex V1({ 10, 9 }), V2({ 10, -2 }), V3({ 21, 1 });
  std::vector<Vertex> s = { V1, V2, V3 };
  Simplex* sm = new Simplex(s);
  Minimizer* nma = new Minimizer;
  cout << nma->NMA(sm) << endl;
#endif
#ifndef TEST
  //#define GAUSS
  //#define SIMPLEX

  Dye C101("../data_cleaned/f4_c101");
  Dye C102("../data_cleaned/f4_c102");
  Dye C103("../data_cleaned/f4_c103");

  Dye C51("../data_cleaned/f4_c51");
  Dye C61("../data_cleaned/f4_c61");
  std::vector<Dye> dyes = { C51, C61 };
  Vertex V1({ 0.5, 0.5 }), V2({ 0.5, 0.55 }), V3({ 0.55, 0.5 });
  std::vector<Vertex> vertices = { V1, V2, V3 };
  Simplex simplex(vertices);

  //  Dye C201("../data_cleaned/f2_c201");
  //  Dye C202("../data_cleaned/f2_c202");
  //  Dye C203("../data_cleaned/f2_c203");
  //  Dye C204("../data_cleaned/f2_c204");
  //  Dye C205("../data_cleaned/f2_c205");

  //  Dye C21("../data_cleaned/f2_c21");
  //  Dye C31("../data_cleaned/f2_c31");
  //  Dye C41("../data_cleaned/f2_c41");
  //  std::vector<Dye> dyes = { C21, C31, C41 };
  //  Vertex V1({ 0.5, 0.5, 0.5 }), V2({ 0.5, 0.55, 0.5 }), V3({ 0.55, 0.5, 0.5
  //  }),
  //    V4({ 0.5, 0.5, 0.55 });

  //  std::vector<Vertex> vertices = { V1, V2, V3, V4 };
  //  Simplex simplex(vertices);

#ifdef GAUSS
  std::vector<Dye> dyes;
  for (auto& path : paths) {
    Dye dye(path);
    dyes.push_back(dye);
  }

  std::vector coords = { 0.1, 0.1 };
  Vertex V1(coords);

  Gauss* gauss = new Gauss;

  Vertex V_ = Vertex();

  std::ofstream file;

  file.open("C101.dat", ios_base::out);
  for (int n = 0; n < 250; n++) {
    Vertex vertex = gauss->optimize(C101.getFullSpectre().at(n), dyes, V1);
    double sigma_ = gauss->sigma(C101.getFullSpectre().at(n), dyes, vertex);

    file << n << "," << vertex << sigma_ << endl;
    cout << n << "," << vertex << sigma_ << endl;
  }
  file.close();

  file.open("C102.dat", ios_base::out);
  for (int n = 0; n < 250; n++) {
    Vertex vertex = gauss->optimize(C102.getFullSpectre().at(n), dyes, V1);
    double sigma_ = gauss->sigma(C102.getFullSpectre().at(n), dyes, vertex);

    file << n << "," << vertex << sigma_ << endl;
    cout << n << "," << vertex << sigma_ << endl;
  }
  file.close();

  file.open("C103.dat", ios_base::out);
  for (int n = 0; n < 250; n++) {
    Vertex vertex = gauss->optimize(C103.getFullSpectre().at(n), dyes, V1);
    double sigma_ = gauss->sigma(C103.getFullSpectre().at(n), dyes, vertex);

    file << n << "," << vertex << sigma_ << endl;
    cout << n << "," << vertex << sigma_ << endl;
  }
  file.close();

  //  V_ = gauss->optimize(C101.average, dyes, V1);
  //  cout << "C101: \t" << V_ << endl;

  //  V1 = Vertex(coords);
  //  V_ = gauss->optimize(C102.average, dyes, V1);
  //  cout << "C102: \t" << V_ << endl;

  //  V1 = Vertex(coords);
  //  V_ = gauss->optimize(C103.average, dyes, V1);
  //  cout << "C103: \t" << V_ << endl;

  //  Dye C201("../data_cleaned/f2_c201");
  //  Dye C202("../data_cleaned/f2_c202");
  //  Dye C203("../data_cleaned/f2_c203");
  //  Dye C204("../data_cleaned/f2_c204");
  //  Dye C205("../data_cleaned/f2_c205");

  //  paths = { "../data_cleaned/f2_c21_after",
  //            "../data_cleaned/f2_c31_upscaled",
  //            "../data_cleaned/f2_c41_upscaled" };

  //  dyes.clear();
  //  for (auto& path : paths) {
  //    Dye dye(path);
  //    dyes.push_back(dye);
  //  }

  //  coords = { 0.1, 0.1, 0.1 };

  //  V1 = Vertex(coords);
  //  V_ = gauss->optimize(C201.average, dyes, V1);
  //  cout << "C201: \t" << V_ << endl;

  //  V1 = Vertex(coords);
  //  V_ = gauss->optimize(C202.average, dyes, V1);
  //  cout << "C202: \t" << V_ << endl;

  //  V1 = Vertex(coords);
  //  V_ = gauss->optimize(C203.average, dyes, V1);
  //  cout << "C203: \t" << V_ << endl;

  //  V1 = Vertex(coords);
  //  V_ = gauss->optimize(C204.average, dyes, V1);
  //  cout << "C204: \t" << V_ << endl;

  //  V1 = Vertex(coords);
  //  V_ = gauss->optimize(C205.average, dyes, V1);
  //  cout << "C205: \t" << V_ << endl;

#endif

#ifdef SIMPLEX
  //  Dye C101("../data_cleaned/f4_c101");
  //  Dye C102("../data_cleaned/f4_c102");
  //  Dye C103("../data_cleaned/f4_c103");

  //  std::vector<std::string> paths;
  //  paths = { "../data_cleaned/f4_c51", "../data_cleaned/f4_c61" };

  std::vector<Dye> dyes;
  for (auto& path : paths) {
    Dye dye(path);
    dyes.push_back(dye);
  }

  std::vector coords = { 0.1, 0.1 };
  Vertex V1(coords);

  coords = { 0.9, 0.9 };
  Vertex V2(coords);

  coords = { 0.5, 0.5 };
  Vertex V3(coords);

  std::vector<Vertex> vertices = { V1, V2, V3 };
  Simplex* simplex = new Simplex(vertices);

  Minimizer* nma = new Minimizer;

  //  cout << nma->NMA(C101.average, dyes, simplex) << "\n\n" << endl;

  std::ofstream file;

  cout << "C101\n";
  file.open("C101_simplex.dat", ios_base::out);
  for (int n = 0; n < 250; n++) {
    Vertex vertex = nma->NMA(C101.getFullSpectre().at(n), dyes, simplex);
    double sigma_ = nma->sigma(C101.getFullSpectre().at(n), dyes, vertex);

    file << n << "," << vertex << sigma_ << endl;
    cout << n << "," << vertex << sigma_ << endl;
  }
  file.close();

  cout << "C102\n";
  *simplex = Simplex(vertices);
  //  cout << nma->NMA(C102.average, dyes, simplex) << "\n\n" << endl;
  file.open("C102_simplex.dat", ios_base::out);
  for (int n = 0; n < 250; n++) {
    Vertex vertex = nma->NMA(C102.getFullSpectre().at(n), dyes, simplex);
    double sigma_ = nma->sigma(C102.getFullSpectre().at(n), dyes, vertex);

    file << n << "," << vertex << sigma_ << endl;
    cout << n << "," << vertex << sigma_ << endl;
  }
  file.close();

  cout << "C103\n";
  *simplex = Simplex(vertices);
  //  cout << nma->NMA(C103.average, dyes, simplex) << "\n\n" << endl;
  file.open("C103_simplex.dat", ios_base::out);
  for (int n = 0; n < 250; n++) {
    Vertex vertex = nma->NMA(C103.getFullSpectre().at(n), dyes, simplex);
    double sigma_ = nma->sigma(C103.getFullSpectre().at(n), dyes, vertex);

    file << n << "," << vertex << sigma_ << endl;
    cout << n << "," << vertex << sigma_ << endl;
  }

//  cout << "\n\n-------------------\n----------------\n\n";

//  Dye C201("../data_cleaned/f2_c201");
//  Dye C202("../data_cleaned/f2_c202");
//  Dye C203("../data_cleaned/f2_c203");
//  Dye C204("../data_cleaned/f2_c204");
//  Dye C205("../data_cleaned/f2_c205");

//  paths.clear();
//  paths = { "../data_cleaned/f2_c21_after",
//            "../data_cleaned/f2_c31_after",
//            "../data_cleaned/f2_c41_after" };

//  dyes.clear();
//  for (auto& path : paths) {
//    Dye dye(path);
//    dyes.push_back(dye);
//  }

//  coords = { 0.1, 0.1, 0 };
//  V1 = Vertex(coords);

//  coords = { 0.9, 0.5, 0 };
//  V2 = Vertex(coords);

//  coords = { 0.1, 0.9, 0 };
//  V3 = Vertex(coords);

//  coords = { 0.5, 0.5, 0.9 };
//  Vertex V4(coords);

//  vertices = { V1, V2, V3, V4 };
//  *simplex = Simplex(vertices);

//  cout << "C201\n";
//  cout << nma->NMA(C201.average, dyes, simplex) << "\n\n" << endl;

//  cout << "C202\n";
//  *simplex = Simplex(vertices);
//  cout << nma->NMA(C202.average, dyes, simplex) << "\n\n" << endl;

//  cout << "C203\n";
//  *simplex = Simplex(vertices);
//  cout << nma->NMA(C203.average, dyes, simplex) << "\n\n" << endl;

//  cout << "C204\n";
//  *simplex = Simplex(vertices);
//  cout << nma->NMA(C204.average, dyes, simplex) << "\n\n" << endl;

//  cout << "C205\n";
//  *simplex = Simplex(vertices);
//  cout << nma->NMA(C205.average, dyes, simplex) << "\n\n" << endl;
#endif

  /*
  optimize(eps, C201.average, dyes);
  cout << "\n\nC202\n";
  optimize(eps, C202.average, dyes);
  cout << "\n\nC203\n";
  optimize(eps, C203.average, dyes);
  cout << "\n\nC204\n";
  optimize(eps, C204.average, dyes);
  cout << "\n\nC205\n";
  optimize(eps, C205.average, dyes);

  cout << "\n\n----------------\n\n";
  */
  /*
    paths = { "../data_cleaned/f2_c21_after",
              "../data_cleaned/f2_c31_after",
              "../data_cleaned/f2_c41_after" };

    dyes.clear();
    for (auto& path : paths) {
      Dye dye(path);
      dyes.push_back(dye);
    }

    cout << "C201\n";
    optimize(eps, C201.average, dyes);
    cout << "\n\nC202\n";
    optimize(eps, C202.average, dyes);
    cout << "\n\nC203\n";
    optimize(eps, C203.average, dyes);
    cout << "\n\nC204\n";
    optimize(eps, C204.average, dyes);
    cout << "\n\nC205\n";
    optimize(eps, C205.average, dyes);

    cout << "\n\n----------------\n\n";

    paths = { "../data_cleaned/f2_c21_after",
              "../data_cleaned/f2_c31_upscaled",
              "../data_cleaned/f2_c41_upscaled" };

    dyes.clear();
    for (auto& path : paths) {
      Dye dye(path);
      dyes.push_back(dye);
    }

    cout << "C201\n";
    optimize(eps, C201.average, dyes);
    cout << "\n\nC202\n";
    optimize(eps, C202.average, dyes);
    cout << "\n\nC203\n";
    optimize(eps, C203.average, dyes);
    cout << "\n\nC204\n";
    optimize(eps, C204.average, dyes);
    cout << "\n\nC205\n";
    optimize(eps, C205.average, dyes);

    cout << "\n\n----------------\n\n"
         << "\n\n----------------\n\n"
         << "\n\n----------------\n\n";

  */

  Gauss* gauss = new Gauss;
  Minimizer* nma = new Minimizer;

  Simplex simplex_old = simplex;
  Vertex vertex_old = V1;

  std::vector<spectre1d_t> mix = C103.getFullSpectre();

  int n = 0;

  std::ofstream file;

  cout << "C103\n";
  file.open("C103.dat", ios_base::out);
  file << "n"
       << ","
       << "gauss_k1"
       << ","
       << "gauss_k2"
       << ","
       << "gauss_sigma"
       << ","
       << "simplex_k1"
       << ","
       << "simplex_k2"
       << ","
       << "simplex_sigma" << endl;

  for (auto& spectre : mix) {

    auto start = chrono::high_resolution_clock::now();
    Vertex vertex_gauss = gauss->optimize(spectre, dyes, vertex_old);
    double sigma_gauss = sigma(spectre, dyes, vertex_gauss);
    vertex_old = vertex_gauss;
    auto stop = chrono::high_resolution_clock::now();

    chrono::duration<double> duration_gauss = stop - start;

    start = chrono::high_resolution_clock::now();
    Simplex simplex = nma->NMA(spectre, dyes, &simplex_old);
    Vertex vertex_simplex = simplex.m_vertices.at(0);
    double sigma_simplex = sigma(spectre, dyes, vertex_simplex);
    simplex_old = simplex;
    simplex_old.m_vertices.at(1).m_coords.at(1) += 0.05;
    simplex_old.m_vertices.at(2).m_coords.at(0) += 0.05;
    stop = chrono::high_resolution_clock::now();

    chrono::duration<double> duration_simplex = stop - start;

    file << n << "," << vertex_gauss << sigma_gauss << "," << vertex_simplex
         << sigma_simplex << endl;
    cout << n << "," << vertex_gauss << sigma_gauss << "," << vertex_simplex
         << sigma_simplex << endl;
    cout << "time elapsed:\t"
         << "gauss\t" << duration_gauss.count() << " s\tsimplex\t"
         << duration_simplex.count() << " s" << endl
         << endl;
    //    file << n << "," << vertex_simplex << sigma_simplex << endl;
    //    cout << n << "," << vertex_simplex << sigma_simplex << endl;
    //    cout << "time elapsed\t" << duration_simplex.count() << " s" << endl;

    n++;
  }
  file.close();
#endif
  return 0;
}
