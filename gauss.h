#ifndef GAUSS_H
#define GAUSS_H

#include "dyes.h"
#include "vertex.h"

class Gauss
{
public:
  Gauss();

  inline double c(double a, double b)
  {
    return ((3 - std::sqrt(5)) / 2 * (b - a)) + a;
  }

  inline double d(double a, double b)
  {
    return ((std::sqrt(5) - 1) / 2 * (b - a)) + a;
  }

  inline double f_x(double a, double x, double b) { return a * x + b; }

  Vertex optimize(spectre1d_t mix, std::vector<Dye> dyes, Vertex vertex);

  double optimize1d(spectre1d_t mix,
                    std::vector<Dye> dyes,
                    Vertex vertex,
                    int fix_idx);
};

#endif // GAUSS_H
