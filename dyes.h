#ifndef DYES_H
#define DYES_H

#include "vertex.h"

#include <cmath>
#include <map>
#include <string>
#include <vector>

const double eps = 0.0001;

// for C10x spectres
const int wavelength_start_idx = 130;
const int wavelength_stop_idx = 200;

// for C20x spectres
// const int wavelength_start_idx = 80;
// const int wavelength_stop_idx = 130;

// all spectre
// const int wavelength_start_idx = 0;
// const int wavelength_stop_idx = 288;

/*!
 * \brief spectre1d_t
 * предполагается, что первое число - длина волны (или ее индекс),
 * втоая - интенсивность
 */
typedef std::map<double, double> spectre1d_t;

/*!
 * \brief The Dye class
 */
class Dye
{
private:
  const std::vector<double> wavelengths = {
    320.469, 323.166, 325.861, 328.553, 331.243, 333.930, 336.615, 339.297,
    341.976, 344.653, 347.327, 349.998, 352.666, 355.331, 357.994, 360.653,
    363.309, 365.962, 368.612, 371.259, 373.903, 376.543, 379.180, 381.813,
    384.443, 387.070, 389.693, 392.313, 394.929, 397.541, 400.150, 402.755,
    405.356, 407.954, 410.547, 413.137, 415.723, 418.305, 420.883, 423.457,
    426.027, 428.593, 431.154, 433.712, 436.265, 438.814, 441.359, 443.900,
    446.436, 448.967, 451.495, 454.018, 456.536, 459.050, 461.559, 464.064,
    466.564, 469.060, 471.551, 474.037, 476.518, 478.995, 481.467, 483.934,
    486.396, 488.853, 491.305, 493.753, 496.195, 498.632, 501.065, 503.492,
    505.914, 508.332, 510.743, 513.150, 515.552, 517.948, 520.339, 522.725,
    525.106, 527.481, 529.851, 532.216, 534.575, 536.929, 539.277, 541.620,
    543.957, 546.289, 548.616, 550.936, 553.252, 555.561, 557.865, 560.164,
    562.457, 564.744, 567.025, 569.301, 571.571, 573.835, 576.093, 578.346,
    580.593, 582.834, 585.069, 587.299, 589.522, 591.740, 593.952, 596.158,
    598.358, 600.551, 602.739, 604.922, 607.098, 609.268, 611.432, 613.590,
    615.742, 617.887, 620.027, 622.161, 624.289, 626.410, 628.526, 630.635,
    632.738, 634.835, 636.926, 639.011, 641.089, 643.162, 645.228, 647.288,
    649.341, 651.389, 653.430, 655.465, 657.494, 659.516, 661.532, 663.542,
    665.546, 667.543, 669.535, 671.519, 673.498, 675.470, 677.436, 679.396,
    681.349, 683.296, 685.236, 687.171, 689.099, 691.020, 692.935, 694.844,
    696.747, 698.643, 700.533, 702.416, 704.293, 706.164, 708.029, 709.887,
    711.738, 713.584, 715.423, 717.255, 719.082, 720.901, 722.715, 724.522,
    726.323, 728.117, 729.905, 731.687, 733.462, 735.232, 736.994, 738.751,
    740.501, 742.244, 743.982, 745.713, 747.437, 749.156, 750.868, 752.573,
    754.273, 755.966, 757.653, 759.333, 761.008, 762.676, 764.337, 765.993,
    767.642, 769.285, 770.922, 772.552, 774.176, 775.795, 777.406, 779.012,
    780.611, 782.205, 783.792, 785.373, 786.947, 788.516, 790.078, 791.635,
    793.185, 794.729, 796.267, 797.799, 799.325, 800.844, 802.358, 803.866,
    805.368, 806.863, 808.353, 809.836, 811.314, 812.786, 814.252, 815.711,
    817.165, 818.613, 820.055, 821.491, 822.922, 824.346, 825.765, 827.178,
    828.585, 829.986, 831.381, 832.771, 834.155, 835.533, 836.906, 838.272,
    839.633, 840.989, 842.339, 843.683, 845.021, 846.354, 847.682, 849.004,
    850.320, 851.631, 852.936, 854.236, 855.530, 856.819, 858.102, 859.380,
    860.653, 861.920, 863.182, 864.439, 865.690, 866.936, 868.177, 869.412,
    870.642, 871.867, 873.087, 874.302, 875.512, 876.716, 877.916, 879.110,
    880.299, 881.483, 882.663, 883.837, 885.006, 886.170, 887.330, 888.484
  };

  std::vector<spectre1d_t> spectreFull;
  spectre1d_t spectreReference;
  int n = 288;

public:
  //! усредненный спектр
  spectre1d_t average;
  std::string code;

  Dye(std::string path);
  Dye(std::string path, int batch);

  std::vector<spectre1d_t> loadSpectre(int wvs, std::string file);
  std::vector<spectre1d_t> loadSpectre(int wvs, int batch, std::string file);
  void filterReference(void);
  spectre1d_t calcAverage();

  inline std::vector<spectre1d_t> getFullSpectre() { return spectreFull; }
};

/*!
 * @brief метрика сходимости
 *
 * @param mix
 * @param dyes
 * @return double
 */
inline double
sigma(spectre1d_t mix, std::vector<Dye> dyes, Vertex vertex)
{

  //! сумма квадратов разности
  double result = 0;
  //! значение функции, аппроксимирующее наблюдение
  double approx = 0;

  for (int wv = wavelength_start_idx; wv < wavelength_stop_idx; wv++) {
    approx = 0;
    for (size_t idx_dye = 0; idx_dye < dyes.size(); idx_dye++)
      approx += dyes.at(idx_dye).average.at(wv) * vertex.m_coords.at(idx_dye);

    result += powf(mix.at(wv) - approx, 2);
  }

  return result;
}

#endif
