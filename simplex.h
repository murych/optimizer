#ifndef SIMPLEX_H
#define SIMPLEX_H

#include "vertex.h"

#include <iostream>

//! @todo: связать с размерностью массива dyes
const int N_DIM = 3;

//! @todo переделать передачу координат в вектор
class Simplex {
public:
  std::vector<Vertex> m_vertices;

  Simplex(std::vector<Vertex> vertices);

  friend std::ostream &operator<<(std::ostream &out, const Simplex &simplex);
};

#endif // SIMPLEX_H
