#ifndef UTILS_H
#define UTILS_H

#include <vector>

/**
 * @brief Слияние элементов.
 * @param[in/out] buf - массив
 * @param[in]     left - левая граница. При певой итерации left = 0
 * @param[in]     right - правая граница. При первой итерации right = buf.size()
 * - 1
 * @param[in]     middle - граница подмассивов.
 *
 * Массив buf содержит два отсортированных подмассива:
 * - [left; middle] - первый отсортированный подмассив.
 * - [middle+1; right] - второй отсортированный подмассив.
 * В результате получаем отсортированный массив, полученный из двух подмассивов,
 * который сохраняется в buf[left; right].
 */
template<typename Type>
static void
merge(std::vector<Type>& buf, size_t left, size_t right, size_t middle)
{
  if (left >= right || middle < left || middle > right)
    return;
  if (right == left + 1 && buf.at(left) > buf.at(right)) {
    std::swap(buf.at(left), buf.at(right));
    return;
  }

  std::vector<Type> tmp(&buf.at(left), &buf.at(right) + 1);

  for (size_t i = left, j = 0, k = middle - left + 1; i <= right; ++i) {
    if (j > middle - left) {
      buf.at(i) = tmp.at(k++);
    } else if (k > right - left) {
      buf.at(i) = tmp.at(j++);
    } else {
      buf.at(i) = (tmp.at(j++) < tmp.at(k++)) ? tmp.at(j++) : tmp.at(k++);
    }
  }
}

/**
 * @brief         Сортировка элементов от left до right массива buf
 * @param[in/out] buf - сортируемый массив
 * @param[in]     left - левая граница. При первой итерации left = 0
 * @param[in]     right - правая граница. При первой итерации right = buf.size()
 * - 1
 *
 * В результате сортируются все элементы массива buf от left до right
 * включительно.
 */
template<typename Type>
void
mergeSort(std::vector<Type>& buf, size_t left, size_t right)
{
  // условие выхода из рекурсии
  if (left >= right)
    return;

  size_t middle = left + (right - left) / 2;

  mergeSort(buf, left, middle);
  mergeSort(buf, middle + 1, right);
  merge(buf, left, right, middle);
}

#endif // UTILS_H
