#include "vertex.h"

#include <cassert>
#include <cmath>
#include <iostream>
#include <numeric>

/*!
 * \brief Vertex::Vertex
 * инициализация n-мерной вершины
 * \param coords
 */
Vertex::Vertex(std::vector<double> coords)
{
  for (auto& n : coords)
    this->m_coords.push_back(n);
}

Vertex
Vertex::operator+=(const Vertex& v)
{
  // проверка, совпадают ли размерности
  assert(this->m_coords.size() == v.m_coords.size());

  for (size_t n = 0; n < this->m_coords.size(); n++)
    this->m_coords.at(n) += v.m_coords.at(n);

  return *this;
}

Vertex
Vertex::operator+(const Vertex& v)
{
  // проверка, совпадают ли размерности
  assert(this->m_coords.size() == v.m_coords.size());

  std::vector<double> coords_scaled = this->m_coords;
  for (size_t n = 0; n < coords_scaled.size(); n++)
    coords_scaled.at(n) += v.m_coords.at(n);

  return *(new Vertex(coords_scaled));
}

Vertex
Vertex::operator-(const Vertex& v)
{
  //проверка, совпадают ли размерности
  assert(this->m_coords.size() == v.m_coords.size());

  std::vector<double> coords_scaled = this->m_coords;
  for (size_t n = 0; n < coords_scaled.size(); n++)
    coords_scaled.at(n) -= v.m_coords.at(n);

  return *(new Vertex(coords_scaled));
}

Vertex
Vertex::operator*(const double d)
{
  std::vector<double> coords_scaled = this->m_coords;
  for (auto& n : coords_scaled)
    n *= d;

  return *(new Vertex(coords_scaled));
}

Vertex
Vertex::operator/(const double q)
{
  std::vector<double> coords_scaled = this->m_coords;
  for (auto& n : coords_scaled)
    n /= q;

  return *(new Vertex(coords_scaled));
}

std::ostream&
operator<<(std::ostream& out, const Vertex& vertex)
{
  for (auto& n : vertex.m_coords)
    out << n << ",";
  return out;
}

double
Vertex::dist(const Vertex& v)
{
  std::vector<double> squares;
  for (size_t n = 0; n < this->m_coords.size(); n++) {
    double a = this->m_coords.at(n), b = v.m_coords.at(n);
    double square = std::pow(b - a, 2);
    squares.push_back(square);
  }

  double square = std::accumulate(squares.begin(), squares.end(), 0.0);
  return std::sqrt(square);
}

bool
Vertex::isNeg()
{
  for (auto& coord : this->m_coords)
    if (coord < 0)
      return true;

  return false;
}
