#include "minimizer.h"

#include <algorithm>
#include <utility>

bool
Minimizer::quitCase(Simplex* s)
{
  for (size_t i = 0; i < s->m_vertices.size(); i++)
    for (size_t j = 0; j < s->m_vertices.size(); j++)
      if (s->m_vertices.at(i).dist(s->m_vertices.at(j)) > eps)
        return false;

  return true;
}

void
Minimizer::sort(Simplex* simplex, std::vector<double> f)
{
  for (size_t idx_i = 0; idx_i < f.size(); idx_i++) {
    for (size_t idx_j = 0; idx_j < f.size() - idx_i - 1; idx_j++) {
      if (f.at(idx_j + 1) < f.at(idx_j)) {
        std::swap(f.at(idx_j), f.at(idx_j + 1));
        std::swap(simplex->m_vertices.at(idx_j),
                  simplex->m_vertices.at(idx_j + 1));
      }
    }
  }
}

Simplex
Minimizer::NMA(spectre1d_t mix, std::vector<Dye> dyes, Simplex* simplex)
{
  std::vector<double> f; //[N_DIM + 1];
  int i = 0;
  bool flag, subzero = false;
  Simplex* simplex_old = simplex;

  // вычисление значений функции на начальном симплексе
  for (auto& vertex : simplex_old->m_vertices)
    f.push_back(sigma(mix, dyes, vertex));

  // проверка на условие выхода
  while (!quitCase(simplex_old)) {
    /*
  for (auto& vertex : simplex->m_vertices)
    for (auto& coord : vertex.m_coords)
      if (coord < 0)
        subzero = true;
*/
    // шаг 1 сортировка
    for (size_t idx_i = 0; idx_i < f.size(); idx_i++) {
      for (size_t idx_j = 0; idx_j < f.size() - idx_i - 1; idx_j++) {
        if (f.at(idx_j + 1) < f.at(idx_j)) {
          std::swap(f.at(idx_j), f.at(idx_j + 1));
          std::swap(simplex_old->m_vertices.at(idx_j),
                    simplex_old->m_vertices.at(idx_j + 1));
        }
      }
    }

    this->x_h = simplex_old->m_vertices.back();
    this->f_h = f.back();

    this->x_g = simplex_old->m_vertices.rbegin()[1];
    this->f_g = f.rbegin()[1];

    this->x_l = simplex_old->m_vertices.front();
    this->f_l = f.front();

    // шаг 2 вычисление центра тяжести симплекса
    this->x_c.m_coords = *(new std::vector<double>);
    for (size_t n = 0; n < simplex_old->m_vertices.size() - 1; n++)
      this->x_c.m_coords.push_back(0);

    for (size_t n = 0; n < simplex_old->m_vertices.size() - 1; n++)
      this->x_c += simplex_old->m_vertices.at(n);

    this->x_c = this->x_c / (simplex_old->m_vertices.size() - 1);

    // шаг 3 отражение
    this->x_r = this->x_c * (1 + this->m_alpha) - this->x_h * m_alpha;
    this->f_r = sigma(mix, dyes, this->x_r);

    // шаг 4
    if (this->f_r <= this->f_l) {
      this->x_e = this->x_c * (1 - this->m_gamma) + this->x_r * this->m_gamma;
      this->f_e = sigma(mix, dyes, this->x_e);

      if (this->f_e < this->f_l) {
        simplex_old->m_vertices.back() = this->x_e;
        f.back() = this->f_e;
      } else {
        simplex_old->m_vertices.back() = this->x_r;
        f.back() = this->f_r;
      }
    }

    if ((this->f_l < this->f_r) && (this->f_r <= this->f_g)) {
      simplex_old->m_vertices.back() = this->x_r;
      f.back() = this->f_r;
    }

    flag = false;

    if ((this->f_h >= this->f_r) && (this->f_r > this->f_g)) {
      flag = true;
      this->tempD = this->f_h;
      this->tempV = this->x_h;

      simplex_old->m_vertices.back() = this->x_r;
      f.back() = this->f_r;

      this->x_r = this->tempV;
      this->f_r = this->tempD;
    }

    if (this->f_r > this->f_h)
      flag = true;

    if (flag) {
      // шаг 5 сжатие
      this->x_s = this->x_h * this->m_beta + this->x_c * (1 - this->m_beta);
      this->f_s = sigma(mix, dyes, this->x_s);

      if (this->f_s < this->f_h) {
        // шаг 6
        this->tempD = this->f_h;
        this->tempV = this->x_h;

        simplex_old->m_vertices.back() = this->x_s;
        f.back() = this->f_s;

        this->x_s = this->tempV;
        this->f_s = this->tempD;
      } else {
        // шаг 7 глобальное сжатие
        for (auto& vertex : simplex_old->m_vertices)
          vertex = this->x_l + (vertex - this->x_l) / 2;
      }
    }

    //    std::cout << "\n-----------" << i << "\n"
    //              << *simplex << "sigma\t" << this->f_l << std::endl;
    //    std::cout << "fl\t" << this->f_l << "\tfh\t" << this->f_h <<
    //    std::endl;

    for (auto& coord : this->x_l.m_coords)
      if (coord < 0) {
        simplex_old = simplex;
        subzero = true;
      }

    if (subzero)
      return *simplex;
  }
  //  std::cout << "\n------------\n" << this->x_l << std::endl;
  //  std::vector<Vertex> rs = { x_l, x_g, x_h };
  //  Simplex result(rs);

  //  std::cout << std::endl
  //            << x_l << "\t" << f_l << ";\t" << x_g << "\t" << f_g << ";\t" <<
  //            x_h
  //            << "\t" << f_h << std::endl
  //            << "current simplex\t" << *simplex << std::endl
  //            << std::endl;

  return *simplex;
}

//#define TEST
#ifdef TEST
double
Func(Vertex v)
{
  double x = v.m_coords.at(0);
  double y = v.m_coords.at(1);
  double result = 100 * (y - x * x) * (y - x * x) + (1 - x) * (1 - x);
  return result;
}

Simplex
Minimizer::NMA(Simplex* simplex)
{
  std::vector<double> f; //[N_DIM + 1];
  int i = 0;
  bool flag;

  // вычисление значений функции на начальном симплексе
  for (auto& vertex : simplex->m_vertices)
    f.push_back(Func(vertex));

  // проверка на условие выхода
  while (!quitCase(simplex)) {
    /*
  for (auto& vertex : simplex->m_vertices)
    for (auto& coord : vertex.m_coords)
      if (coord < 0)
        subzero = true;
*/
    // шаг 1 сортировка
    for (size_t idx_i = 0; idx_i < f.size(); idx_i++) {
      for (size_t idx_j = 0; idx_j < f.size() - idx_i - 1; idx_j++) {
        if (f.at(idx_j + 1) < f.at(idx_j)) {
          std::swap(f.at(idx_j), f.at(idx_j + 1));
          std::swap(simplex->m_vertices.at(idx_j),
                    simplex->m_vertices.at(idx_j + 1));
        }
      }
    }

    this->x_h = simplex->m_vertices.back();
    this->f_h = f.back();

    this->x_g = simplex->m_vertices.rbegin()[1];
    this->f_g = f.rbegin()[1];

    this->x_l = simplex->m_vertices.front();
    this->f_l = f.front();

    // шаг 2 вычисление центра тяжести симплекса
    this->x_c.m_coords = { 0, 0 };
    for (size_t n = 0; n < simplex->m_vertices.size() - 1; n++)
      this->x_c += simplex->m_vertices.at(n);
    this->x_c = this->x_c / (simplex->m_vertices.size() - 1);

    // шаг 3 отражение
    this->x_r = this->x_c * (1 + this->m_alpha) - this->x_h * m_alpha;
    this->f_r = Func(this->x_r);

    // шаг 4
    if (this->f_r <= this->f_l) {
      this->x_e = this->x_c * (1 - this->m_gamma) + this->x_r * this->m_gamma;
      this->f_e = Func(this->x_e);

      if (this->f_e < this->f_l) {
        simplex->m_vertices.back() = this->x_e;
        f.back() = this->f_e;
      } else {
        simplex->m_vertices.back() = this->x_r;
        f.back() = this->f_r;
      }
    }

    if ((this->f_l < this->f_r) && (this->f_r <= this->f_g)) {
      simplex->m_vertices.back() = this->x_r;
      f.back() = this->f_r;
    }

    flag = false;

    if ((this->f_h >= this->f_r) && (this->f_r > this->f_g)) {
      flag = true;
      this->tempD = this->f_h;
      this->tempV = this->x_h;

      simplex->m_vertices.back() = this->x_r;
      f.back() = this->f_r;

      this->x_r = this->tempV;
      this->f_r = this->tempD;
    }

    if (this->f_r > this->f_h)
      flag = true;

    if (flag) {
      this->x_s = this->x_h * this->m_beta + this->x_c * (1 - this->m_beta);
      this->f_s = Func(this->x_s);

      if (this->f_s < this->f_h) {
        this->tempD = this->f_h;
        this->tempV = this->x_h;

        simplex->m_vertices.back() = this->x_s;
        f.back() = this->f_s;

        this->x_s = this->tempV;
        this->f_s = this->tempD;
      } else {
        for (auto& vertex : simplex->m_vertices)
          vertex = this->x_l + (vertex - this->x_l) / 2;
      }
    }

    std::cout << "\n-----------" << i << "\n"
              << *simplex << "sigma\t" << this->f_l << std::endl;
    std::cout << "fl\t" << this->f_l << "\tfh\t" << this->f_h << std::endl;

    /*
    for (auto& coord : this->x_l.m_coords)
      if (coord < 0) {
        simplex = simplex_old;
        subzero = true;
      }

    if (subzero)
      break;
    */
    i++;
  }
  //  std::cout << "\n------------\n" << this->x_l << std::endl;
  return *simplex;
}
#endif // TEST
