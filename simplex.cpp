#include "simplex.h"

Simplex::Simplex(std::vector<Vertex> vertices)
{
  for (auto& vertex : vertices)
    this->m_vertices.push_back(vertex);
}

std::ostream&
operator<<(std::ostream& out, const Simplex& simplex)
{
  for (auto& vertex : simplex.m_vertices)
    out << vertex;
  return out;
}
