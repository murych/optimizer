#include "dyes.h"

#include <cmath>
#include <fstream>
#include <iostream>
#include <numeric>
#include <sstream>
#include <string>

Dye::Dye(std::string path)
{
  // надо брать значения из ячеек с 80 по 130
  auto const code = path.find_last_of('/');
  this->code = path.substr(code + 1);
  this->spectreFull = loadSpectre(this->n, path + ".csv");
  this->spectreReference = loadSpectre(this->n, path + "_ref.csv").at(0);

  filterReference();
  this->average = calcAverage();
}

Dye::Dye(std::string path, int batch)
{
  auto const code = path.find_last_of('/');
  this->code = path.substr(code + 1);
  this->spectreFull = loadSpectre(this->n, batch, path + ".csv");
  this->spectreReference = loadSpectre(this->n, batch, path + "_ref.csv").at(0);

  filterReference();
  this->average = calcAverage();
}

/*!
 * @brief
 *
 * @param n
 * @param file
 * @return std::vector<std::map<double, double>>
 */
std::vector<std::map<double, double>>
Dye::loadSpectre(int wvs, std::string file)
{
  std::vector<std::vector<double>> result(wvs);

  std::ifstream input(file);

  std::string s;
  for (int i = 0; i < wvs; i++) {
    std::getline(input, s);
    std::istringstream iss(s);

    std::string num;
    while (std::getline(iss, num, ','))
      result.at(i).push_back(std::stod(num));
  }

  std::vector<std::map<double, double>> spectres;

  // по колонкам (shot)
  for (int column = 0; column < int(result.at(0).size()); column++) {
    std::map<double, double> m;
    // по строчкам (wavelength)
    for (int row = wavelength_start_idx; row < wavelength_stop_idx; row++) {
      m.insert({ row, result.at(row).at(column) });
    }
    spectres.push_back(m);
  }

  return spectres;

  //  return result;
}

std::vector<spectre1d_t>
Dye::loadSpectre(int wvs, int batch, std::string file)
{
  std::vector<std::vector<double>> result(wvs);
  std::ifstream input(file);
  std::string buffer;

  for (int i = 0; i < wvs; i++) {
    std::getline(input, buffer);
    std::istringstream iss(buffer);

    std::string num;
    while (std::getline(iss, num, ','))
      result.at(i).push_back(std::stod(num));
  }

  std::vector<spectre1d_t> spectres;

  for (size_t batch_ = 0; batch_ < result.at(0).size(); batch_ += batch) {
    spectre1d_t m;
    for (int wv = wavelength_start_idx; wv < wavelength_stop_idx; wv++) {
      double shot_averaged = 0;
      for (int shot_ = 0; shot_ < batch; shot_++)
        shot_averaged += result.at(wv).at(batch_ + shot_);
      shot_averaged /= double(batch);
      m.insert({ wv, shot_averaged });
    }
    spectres.push_back(m);
  }
  return spectres;
}

/*!
 * @brief
 *
 */
void
Dye::filterReference()
{
  std::vector<double> result;

  for (auto& sample : this->spectreFull)
    for (int wv = wavelength_start_idx; wv < wavelength_stop_idx; wv++)
      sample.at(wv) -= this->spectreReference.at(wv);

  return;
}

/*!
 * @brief
 *
 * @return std::map<double, double>
 */
spectre1d_t
Dye::calcAverage()
{
  std::map<double, double> result = {};

  for (int wv = wavelength_start_idx; wv < wavelength_stop_idx; wv++) {
    double average = 0;

    for (auto& sample : this->spectreFull) {
      average += sample.at(wv);
    }

    average /= spectreFull.size();

    result.insert({ wv, average });
  }
  return result;
}
