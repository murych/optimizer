#include "gauss.h"

#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>

Gauss::Gauss() {}

Vertex
Gauss::optimize(spectre1d_t mix, std::vector<Dye> dyes, Vertex vertex)
{
  double sigma_old, sigma_ = sigma(mix, dyes, vertex);

  do {
    sigma_old = sigma_;

    for (size_t k = 0; k < vertex.m_coords.size(); k++)
      vertex.m_coords.at(k) = optimize1d(mix, dyes, vertex, k);
    sigma_ = sigma(mix, dyes, vertex);

  } while (fabs(sigma_old - sigma_) > eps);

  //  std::cout << "sigma\t" << sigma_ << std::endl;
  //  std::cout << vertex << sigma_ << std::endl;
  return vertex;
}

double
Gauss::optimize1d(spectre1d_t mix,
                  std::vector<Dye> dyes,
                  Vertex vertex,
                  int fix_idx)
{

  double k1_start = 0, k1_end = 1;
  double a = k1_start, b = k1_end;
  double k_alpha = this->c(a, b);
  double k_beta = this->d(a, b);

  while ((b - a) / 2 > eps) {
    Vertex vertex_alpha = vertex;
    vertex_alpha.m_coords.at(fix_idx) = k_alpha;
    double sigma_alpha = sigma(mix, dyes, vertex_alpha);

    Vertex vertex_beta = vertex;
    vertex_beta.m_coords.at(fix_idx) = k_beta;
    double sigma_beta = sigma(mix, dyes, vertex_beta);

    if (sigma_alpha <= sigma_beta) {
      b = k_beta;
      k_beta = k_alpha;
      k_alpha = this->c(a, b);
    } else {
      a = k_alpha;
      k_alpha = k_beta;
      k_beta = this->d(a, b);
    }
  }
  return (b + a) / 2;
}
